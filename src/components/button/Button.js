import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./Button.scss"

class Button extends Component {

    render() {

        const {children, onClick} = this.props;
        return (
            <button className="button" onClick={onClick}>{children}</button>
        );
    }
}

Button.propType = {
    children: PropTypes.string,
    onClick: PropTypes.func.isRequired

}

Button.defaultTypes = {
    children: "OK"
}

export default Button;