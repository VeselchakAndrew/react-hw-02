import React, {Component} from 'react';
import Card from "../card/Card";
import "./Body.scss"
import Button from "../button/Button";
import Favourite from "../favourite/Favourite";
import Modal from "../modal/Modal";

class Body extends Component {

    state = {
        goods: [],
        goodsInCart: [],
        goodsInFavourite: [],
        showModal: false,
    }

    componentDidMount() {
        const dataBase = `baza.json`;
        fetch(dataBase)
            .then((res) => res.json())
            .then((res) => {
                this.setState({goods: res})
            })


        if (!localStorage.getItem("Favourite")) {
            (localStorage.setItem("Favourite", JSON.stringify([])))
        } else {
            this.setState({
                goodsInFavourite: (JSON.parse(localStorage.getItem("Favourite")))
            });
        }

        if (!localStorage.getItem("Cart")) {
            (localStorage.setItem("Cart", JSON.stringify([])))
        } else {
            this.setState({
                goodsInCart: (JSON.parse(localStorage.getItem("Cart")))
            });
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {goodsInCart, goodsInFavourite} = this.state;
        localStorage.setItem("Cart", JSON.stringify(goodsInCart));
        localStorage.setItem("Favourite", JSON.stringify(goodsInFavourite));
    }

    render() {
        const {goods, showModal, goodsInFavourite} = this.state;

        const goodItems = goods.map(good => (
            <div key={good.vendorCode}>
                <Card good={good}
                      children={
                          <>
                              <Button onClick={() => this.handleClick(good)}>Add to cart</Button>
                              <Favourite
                                  isChecked={goodsInFavourite.find(item => item.vendorCode === good.vendorCode)}
                                  onClick={() => this.addToFavourite(good)}
                              />
                          </>
                      }
                />

            </div>
        ))

        return (
            <div className="main">
                {goodItems}
                {showModal && <Modal onClick={this.closeWindow}/>}
            </div>
        );
    }

    addToCart = (good) => {
        const {goodsInCart} = this.state;
        const checkData = good.vendorCode;

        if (!goodsInCart.find(item => item.vendorCode === checkData)) {
            this.setState({
                goodsInCart: [...goodsInCart, good],
            })
        }
    }

    addToFavourite = (good) => {
        const {goodsInFavourite} = this.state;
        const checkData = good.vendorCode;

        if (!goodsInFavourite.find(item => item.vendorCode === checkData)) {
            this.setState({
                goodsInFavourite: [...goodsInFavourite, good],
            })

        } else {
            const newData = goodsInFavourite.filter((element) => {
                return element.vendorCode !== good.vendorCode
            });
            this.setState({goodsInFavourite: newData});
        }
    }

    showWindow = () => {
        this.setState({showModal: true});
        document.body.classList.add("modal-open");
    }

    handleClick = (good) => {
        this.addToCart(good);
        this.showWindow();
    }

    closeWindow = () => {
        this.setState({showModal: false});
        document.body.classList.remove("modal-open");
    }
}

export default Body;