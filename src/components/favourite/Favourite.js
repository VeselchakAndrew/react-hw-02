import React, {Component} from 'react';
import "./Favourite.scss"

class Favourite extends Component {

    state = {
        inFavourite: false,
    };

    componentDidMount() {
        const {isChecked} = this.props;
        this.setInitialState(isChecked);
    }

    changeIcon = () => {
        this.setState(({inFavourite}) => ({
            inFavourite: !inFavourite
        }));
    }

    clickHandler = () => {
        this.props.onClick();
        this.changeIcon();
    }

    setInitialState = (initialState) => {
        this.setState({
            inFavourite: initialState
        })
    }

    render() {
        const {inFavourite} = this.state;


        let classes = "favourite"
        if (inFavourite) {
            classes += " is_checked"
        }

        return (
            <div className={classes} onClick={this.clickHandler}></div>
        );
    }
}

export default Favourite;