import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./Card.scss";

class Card extends Component {

    render() {
        const {name, price, color, img} = this.props.good;
        const {children} = this.props;

        return (
            <div className="card">
                <div className="card_image_box"><img src={img} alt="Car" className="card_image"></img></div>
                <div>Название: {name}</div>
                <div>Цена: {price}</div>
                <div>Цвет: {color}</div>

                {children}
            </div>
        );
    }

}

Card.propType = {
    vendorCode: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    color: PropTypes.string,
    img: PropTypes.string
}

Card.defaultProps = {
    color: "Уточняйте",
    img: "img/default.img"
}


export default Card;
