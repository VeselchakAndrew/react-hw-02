import React, {PureComponent} from 'react';
import "./Header.scss"

class Header extends PureComponent {
    render() {
        return (
            <div className="header">
                <h1 className="header_title">Hot Wheels</h1>
            </div>
        );
    }
}

export default Header;