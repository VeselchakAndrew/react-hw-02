import React, {Component} from 'react';
import PropTypes from "prop-types";
import Button from "../button/Button";
import "./Modal.scss"

class Modal extends Component {

    render() {
        const {onClick} = this.props;
        return (
            <div onClick={onClick} className="ModalBg">
                <div className="Modal">
                    <div className="Header">
                        <div className="Cross" onClick={onClick}></div>
                    </div>
                    <div className="Main_content">
                        <div>Товар упешно добавлен в корзину!</div>
                        <Button onClick={onClick}>OK</Button>
                    </div>
                </div>

            </div>
        )
    }
}

Modal.propType = {
    onClick: PropTypes.func.isRequired,
}


export default Modal;